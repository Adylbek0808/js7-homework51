import React from 'react';

const Numbox = props => {
    return (
        <div className="NumBox">
            <button type="button" onClick={props.newnums}>New Numbers</button>
            <p>{props.num1}</p>
            <p>{props.num2}</p>
            <p>{props.num3}</p>
            <p>{props.num4}</p>
            <p>{props.num5}</p>
        </div>

    );
};

export default Numbox;